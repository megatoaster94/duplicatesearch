#ifndef __FILEHANDlER__
#define __FILEHANDlER__

#include <string>
#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <openssl/md5.h>

class MyFile
{
public:
    MyFile();
    MyFile(const std::filesystem::path &sPath, std::uintmax_t nSize, const std::array<unsigned char, MD5_DIGEST_LENGTH> &nHash);

    static std::array<unsigned char, MD5_DIGEST_LENGTH> MD5FromFile(const std::string &path);
    static bool FilesEqual(std::filesystem::path p1, std::filesystem::path p2);

    const std::filesystem::path &sPath() const;
    std::uintmax_t nSize() const;
    const std::array<unsigned char, 16> &nHash() const;

private:
    std::filesystem::path                           m_sPath; //relative
    std::uintmax_t                                  m_nSize;
    std::array<unsigned char, MD5_DIGEST_LENGTH>    m_nHash;
};

#endif //__FILEHANDlER__
