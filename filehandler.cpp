#include "filehandler.h"

MyFile::MyFile()
{

}

MyFile::MyFile(const std::filesystem::__cxx11::path &sPath, uintmax_t nSize, const std::array<unsigned char, MD5_DIGEST_LENGTH> &nHash) :
    m_sPath(sPath),
    m_nSize(std::move(nSize)),
    m_nHash(nHash)
{}

std::array<unsigned char, MD5_DIGEST_LENGTH> MyFile::MD5FromFile(const std::string &path)
{
    std::array<unsigned char, MD5_DIGEST_LENGTH> result;

    std::ifstream file(path.c_str(), std::ios::binary | std::ios::in);

    if (file.fail()) {
        std::cerr << "Error opening file!" << std::endl;
        return std::array<unsigned char, MD5_DIGEST_LENGTH>{0};
    }

    file.seekg(0, std::ios::end);
    size_t length = file.tellg();
    file.seekg(0, std::ios::beg);

    char *data = new char[length];
    file.read(data, length);

    MD5((unsigned char*)data, length, result.data());

    delete[] data;

    return result;
}

bool MyFile::FilesEqual(std::filesystem::__cxx11::path p1, std::filesystem::__cxx11::path p2)
{
    bool result = true;
    std::ifstream file1(p1.c_str(), std::ios::binary | std::ios::in);
    std::ifstream file2(p2.c_str(), std::ios::binary | std::ios::in);

    if (file1.fail() || file2.fail()) {
        std::cerr << "Error with opening files: " << p1 << "; " << p2 << std::endl;
        return false;
    }

    while (!file1.eof() && !file2.eof())
    {
        unsigned char byte1;
        unsigned char byte2;

        byte1 = file1.get();
        byte2 = file2.get();

        if (file1.eof() && file2.eof()) {
            break;
        }

        if (file1.fail() || file2.fail())
        {
            std::cerr << "Error while reading files: " << p1 << "; " << p2 << std::endl;
            result = false;
            break;
        }

        if (byte1 != byte2) {
            result = false;
            break;
        }
    }
    return result;
}

const std::filesystem::path &MyFile::sPath() const
{
    return m_sPath;
}

std::uintmax_t MyFile::nSize() const
{
    return m_nSize;
}

const std::array<unsigned char, 16> &MyFile::nHash() const
{
    return m_nHash;
}
