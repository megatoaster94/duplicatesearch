#include <iostream>
#include <thread>
#include <vector>
#include <unordered_map>
#include <map>
#include <algorithm>

#include "filehandler.h"

int main(int argc, char* argv[])
{
    std::vector<MyFile> files;
    std::vector<std::thread> threads;

    std::string initial_path(".");
    int threads_number = 1;

    if (argc == 2) {
        initial_path = argv[1];
    }
    else if (argc == 3) {
        initial_path = argv[1];
        std::string threads_arg = argv[2];
        try {
            std::size_t pos;
            threads_number = std::stoi(threads_arg, &pos);
            if (pos < threads_arg.size()) {
                std::cout << "Trailing characters after number: " << threads_arg << '\n';
            }
        } catch (std::invalid_argument const &ex) {
            std::cerr << "Invalid number of threads: " << threads_arg << '\n';
            exit(1);
        } catch (std::out_of_range const &ex) {
            std::cerr << "Number of threads out of range: " << threads_arg << '\n';
            exit(1);
        }
    }
    else if (argc > 3) {
        std::cerr << "Too many arguments!" << std::endl;
        exit(1);
    }

    //std::cout << "Initial path: " << initial_path << std::endl;

    if (!std::filesystem::exists(initial_path)) {
        std::cerr << "Path doesn't exists!" << std::endl;
        exit(1);
    }

    for (auto &p : std::filesystem::recursive_directory_iterator(initial_path)) {
        if (!std::filesystem::is_directory(p)) {
            MyFile file(p.path(),
                        p.file_size(),
                        MyFile::MD5FromFile(p.path()));
            files.push_back(file);
        }
    }

    typedef std::unordered_map<std::string, std::vector<std::string>> filemap_type;
    filemap_type equal_files;

    for (auto iter = files.begin(); iter != files.end(); ++iter) {
        for (auto jter = iter + 1; jter != files.end(); ++jter) {
            if (iter->nSize() == jter->nSize()) {
                if (iter->nHash() == jter->nHash()) {
                    if (MyFile::FilesEqual(iter->sPath(), jter->sPath())) {
                        equal_files[iter->sPath()].push_back(jter->sPath());
                        files.erase(jter);
                        --jter;
                    }
                }
            }
        }
    }

    if (!equal_files.empty()) {
        //print files
        std::cout << "Equal files" << std::endl;
        for (const auto &first_file : equal_files) {
            std::cout << first_file.first << ":" << std::endl;
            for (const auto &same : first_file.second) {
                std::cout << '\t' << same << std::endl;
            }
        }
    }
}
